<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.13</version>
	</parent>
	<groupId>com.tankfinal</groupId>
	<artifactId>lab-dependencies</artifactId>
	<version>1.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>
	<description>Define spring boot Dependences version. spring boot feature application must import this pom as parent.</description>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.encoding>UTF-8</project.encoding>
		<version.jdk>1.8</version.jdk>
		<version.maven-scm-api>1.11.1</version.maven-scm-api>
		<version.maven-scm-provider-gitexe>1.11.1</version.maven-scm-provider-gitexe>
		<p6spy.version>3.9.1</p6spy.version>
		<liquibase.core.version>3.10.3</liquibase.core.version>
		<hutool-all.version>5.7.3</hutool-all.version>
		<snakeyaml.version>1.28</snakeyaml.version>
		<alibaba.fastjson.version>1.2.75</alibaba.fastjson.version>
		<commons-lang3.version>3.12.0</commons-lang3.version>
		<commons-compress.version>1.20</commons-compress.version>
		<xz.version>1.9</xz.version>
		<mssql-jdbc.version>8.2.2.jre8</mssql-jdbc.version>
		<mysql-connector-j>8.2.0</mysql-connector-j>
		<junit-platform-launcher.version>1.7.1</junit-platform-launcher.version>
		<junit-jupiter-engine.version>5.7.1</junit-jupiter-engine.version>
		<junit-vintage-engine.version>4.12.3</junit-vintage-engine.version>
		<maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
		<version.maven-release-plugin>2.5.3</version.maven-release-plugin>
		<org.projectlombok.version>1.18.20</org.projectlombok.version>
		<spring.cloud.hystrix.version>2.2.7.RELEASE</spring.cloud.hystrix.version>
		<spring.cloud.eureka.client.version>3.0.2</spring.cloud.eureka.client.version>
		<spring.cloud.config.version>3.0.3</spring.cloud.config.version>
		<spring-cloud-commons.version>3.0.2</spring-cloud-commons.version>
		<spring.cloud.openfeign.version>3.0.2</spring.cloud.openfeign.version>
		<spring.boot.major.version>2.4.3</spring.boot.major.version>
		<mybatis-plus.version>3.4.2</mybatis-plus.version>
		<mybatis-plus.generator.version>3.3.2</mybatis-plus.generator.version>
		<mybatis-typehandlers-jsr310.version>1.0.1</mybatis-typehandlers-jsr310.version>
		<commons-collections4.version>4.4</commons-collections4.version>
		<commons-io.version>2.8.0</commons-io.version>
		<flyway-core.version>7.8.2</flyway-core.version>
		<seata-spring-boot-starter.version>1.5.0-SNAPSHOT</seata-spring-boot-starter.version>
		<velocity-engine-core.version>2.3</velocity-engine-core.version>
		<io.springfox.version>3.0.0</io.springfox.version>
		<swagger-bootstrap-ui.version>1.9.6</swagger-bootstrap-ui.version>
		<knife4j-spring-boot-starter.version>3.0.3</knife4j-spring-boot-starter.version>
		<p6spy-spring-boot-starter.version>1.8.0</p6spy-spring-boot-starter.version>
		<gson.version>2.9.0</gson.version>
		<feign-httpclient.version>11.8</feign-httpclient.version>
		<lombok-mapstruct-binding.version>0.2.0</lombok-mapstruct-binding.version>
		<swagger-models.version>1.5.21</swagger-models.version>
		<springfox-swagger2.version>2.9.2</springfox-swagger2.version>
		<springfox-swagger-ui.version>2.9.2</springfox-swagger-ui.version>
	</properties>
	<!-- 版本庫 -->
	<dependencies>
		<!-- spring boot 應用監控，包括監管,審計,收集。建議加上security -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
			<version>${spring.boot.major.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-commons</artifactId>
			<version>${spring-cloud-commons.version}</version>
		</dependency>
		<!-- 網頁相關服務 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<version>${spring.boot.major.version}</version>
		</dependency>
		<!-- spring cloud 熔斷監控。需搭配hystrix-dashboard -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
			<version>${spring.cloud.hystrix.version}</version>
		</dependency>
		<!-- spring cloud Eureka -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
			<version>${spring.cloud.eureka.client.version}</version>
		</dependency>
		<!-- spring cloud 微服務集中配置伺服器 -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
			<version>${spring.cloud.config.version}</version>
		</dependency>
		<!-- spring cloud Feign Client-->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
			<version>${spring.cloud.openfeign.version}</version>
		</dependency>
		<!-- 阿里 JSON -->
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>fastjson</artifactId>
			<version>${alibaba.fastjson.version}</version>
		</dependency>
		<!-- gson -->
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>${gson.version}</version>
		</dependency>
		<!-- Hutool CN -->
		<dependency>
			<groupId>cn.hutool</groupId>
			<artifactId>hutool-all</artifactId>
			<version>${hutool-all.version}</version>
		</dependency>
		<!-- yaml 解析 -->
		<dependency>
			<groupId>org.yaml</groupId>
			<artifactId>snakeyaml</artifactId>
			<version>${snakeyaml.version}</version>
		</dependency>
		<!-- MSSQL驅動-->
<!--		<dependency>-->
<!--			<groupId>com.microsoft.sqlserver</groupId>-->
<!--			<artifactId>mssql-jdbc</artifactId>-->
<!--			<scope>runtime</scope>-->
<!--			<version>${mssql-jdbc.version}</version>-->
<!--		</dependency>-->
		<dependency>
			<groupId>com.mysql</groupId>
			<artifactId>mysql-connector-j</artifactId>
			<version>${mysql-connector-j}</version>
		</dependency>
		<!-- 支持velocity模板引擎 -->
		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity-engine-core</artifactId>
			<version>${velocity-engine-core.version}</version>
		</dependency>
		<!-- apache共用包 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons-lang3.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
			<version>${commons-collections4.version}</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${commons-io.version}</version>
		</dependency>
		<!-- 壓縮檔相關 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-compress</artifactId>
			<version>${commons-compress.version}</version>
		</dependency>
		<!-- lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${org.projectlombok.version}</version>
		</dependency>

		<!-- 壓縮檔選擇的 XZ for Java庫-->
		<dependency>
			<groupId>org.tukaani</groupId>
			<artifactId>xz</artifactId>
			<version>${xz.version}</version>
		</dependency>
		<!-- feign-httpclient 支援 Request 為 Patch的請求 -->
		<dependency>
			<groupId>io.github.openfeign</groupId>
			<artifactId>feign-httpclient</artifactId>
			<version>${feign-httpclient.version}</version>
		</dependency>
		<!-- 解決 mapstruct 與 lombok 的相容性問題 -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok-mapstruct-binding</artifactId>
			<version>${lombok-mapstruct-binding.version}</version>
		</dependency>
	</dependencies>
	<!-- 版本依賴 -->
	<dependencyManagement>
		<dependencies>
			<!-- spring-boot-configuration-processor -->
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-configuration-processor</artifactId>
				<version>${spring.boot.major.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- 任務調度相關代碼 -->
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-quartz</artifactId>
				<version>${spring.boot.major.version}</version>
				<optional>true</optional>
			</dependency>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-data-jpa</artifactId>
				<version>${spring.boot.major.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- 測試服務 -->
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-test</artifactId>
				<version>${spring.boot.major.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- Junit -->
			<dependency>
				<groupId>org.junit.platform</groupId>
				<artifactId>junit-platform-launcher</artifactId>
				<version>${junit-platform-launcher.version}</version>
				<optional>true</optional>
			</dependency>
			<dependency>
				<groupId>org.junit.jupiter</groupId>
				<artifactId>junit-jupiter-engine</artifactId>
				<version>${junit-jupiter-engine.version}</version>
				<optional>true</optional>
			</dependency>
			<dependency>
				<groupId>org.junit.vintage</groupId>
				<artifactId>junit-vintage-engine</artifactId>
				<version>${junit-vintage-engine.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- 資料庫版本控制 -->
			<dependency>
				<groupId>org.liquibase</groupId>
				<artifactId>liquibase-core</artifactId>
				<version>${liquibase.core.version}</version>
				<optional>true</optional>
			</dependency>
			<dependency>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-core</artifactId>
				<version>${flyway-core.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- mybatis -->
			<dependency>
				<groupId>com.baomidou</groupId>
				<artifactId>mybatis-plus-boot-starter</artifactId>
				<version>${mybatis-plus.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- MyBatisPlus 代碼生成 -->
			<dependency>
				<groupId>com.baomidou</groupId>
				<artifactId>mybatis-plus-generator</artifactId>
				<version>${mybatis-plus.generator.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- MyBatis 日期時間類庫支援-->
			<dependency>
				<groupId>org.mybatis</groupId>
				<artifactId>mybatis-typehandlers-jsr310</artifactId>
				<version>${mybatis-typehandlers-jsr310.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- Swagger 3 -->
			<dependency>
				<groupId>io.springfox</groupId>
				<artifactId>springfox-boot-starter</artifactId>
				<version>${io.springfox.version}</version>
				<optional>true</optional>
			</dependency>
			<!--			&lt;!&ndash; Swagger 2 將註解解析成需要使用的Model和一些Model數據處理&ndash;&gt;-->
			<!--			<dependency>-->
			<!--				<groupId>io.swagger</groupId>-->
			<!--				<artifactId>swagger-models</artifactId>-->
			<!--				<version>${swagger-models.version}</version>-->
			<!--				<optional>true</optional>-->
			<!--			</dependency>-->
			<!--			&lt;!&ndash; Swagger2 主要依賴 &ndash;&gt;-->
			<!--			<dependency>-->
			<!--				<groupId>io.springfox</groupId>-->
			<!--				<artifactId>springfox-swagger2</artifactId>-->
			<!--				<version>${springfox-swagger2.version}</version>-->
			<!--				<optional>true</optional>-->
			<!--			</dependency>-->
			<!--			&lt;!&ndash; Swagger2 主要依賴 &ndash;&gt;-->
			<!--			<dependency>-->
			<!--				<groupId>io.springfox</groupId>-->
			<!--				<artifactId>springfox-swagger-ui</artifactId>-->
			<!--				<version>${springfox-swagger-ui.version}</version>-->
			<!--				<optional>true</optional>-->
			<!--			</dependency>-->

			<!-- alibaba Seata 分布式事務管理套件 -->
<!--			<dependency>-->
<!--				<groupId>com.tankfinal.io.seata</groupId>-->
<!--				<artifactId>seata-spring-boot-starter</artifactId>-->
<!--				<version>1.5.0-SNAPSHOT</version>-->
<!--				<optional>true</optional>-->
<!--			</dependency>-->
			<!-- p6spy 原生 -->
			<dependency>
				<groupId>p6spy</groupId>
				<artifactId>p6spy</artifactId>
				<version>${p6spy.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- swagger2 用 bootstrapUI -->
			<dependency>
				<groupId>com.github.xiaoymin</groupId>
				<artifactId>swagger-bootstrap-ui</artifactId>
				<version>${swagger-bootstrap-ui.version}</version>
				<optional>true</optional>
			</dependency>
			<!-- swagger3 用 knife4j -->
			<dependency>
				<groupId>com.github.xiaoymin</groupId>
				<artifactId>knife4j-spring-boot-starter</artifactId>
				<version>${knife4j-spring-boot-starter.version}</version>
				<optional>true</optional>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<build>
		<finalName>${project.artifactId}-${project.version}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin.version}</version>
				<configuration>
					<source>${version.jdk}</source>
					<target>${version.jdk}</target>
					<encoding>${project.encoding}</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>${version.maven-release-plugin}</version>
				<dependencies>
					<dependency>
						<groupId>org.apache.maven.scm</groupId>
						<artifactId>maven-scm-api</artifactId>
						<version>${version.maven-scm-api}</version>
					</dependency>
					<dependency>
						<groupId>org.apache.maven.scm</groupId>
						<artifactId>maven-scm-provider-gitexe</artifactId>
						<version>${version.maven-scm-provider-gitexe}</version>
					</dependency>
				</dependencies>
				<configuration>
					<tagNameFormat>@{project.artifactId}-@{project.version}</tagNameFormat>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<repositories>
		<repository>
			<id>internal-repository</id>
			<url>http://localhost:8081/repository/maven-central/</url>
		</repository>
		<repository>
			<id>snapshot</id>
			<url>http://localhost:8081/repository/lab-snapshot/</url>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy> <!--对发布版本不需要每次更新，因为在nexus中发布版本同一版本号只能提交一次 -->
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy> <!--必须使用该配置，如果在开发环境中，快照版本同一版本号可以提交多次，nexus中是在版本号后加时间戳区分，所以在开发环境中当其它开发人员提交了同一版本的最新修改对于另一开发人员是不知道的，设置了该参数后就会每次去取该版本号中的最新版本 -->
			</snapshots>
		</repository>
	</repositories>
	<distributionManagement>
		<repository>
			<id>release</id>
			<url>http://localhost:8081/repository/lab-release/</url>
		</repository>
		<snapshotRepository>
			<id>snapshot</id>
			<url>http://localhost:8081/repository/lab-snapshot/</url>
		</snapshotRepository>
	</distributionManagement>
</project>
